# ToDoApp

Clone our repo to local
  git clone https://gitlab.com/vyhuit/todoapp.git

Select folder for repo saving.

Open in Vscode.

  git checkout your branch (vd:dev_hoang)

  git pull origin review_branch
  
To run your project, navigate to the directory and run one of the following npm commands.
  cd todoapp
  npm install
  npm start
  npm run android
  npm run ios
  npm run web (run this command for show QR code then scan)

⚠️  Before running your app on iOS, make sure you have CocoaPods installed and initialize the project:

  cd todoapp/ios
  npx pod-install