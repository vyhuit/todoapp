import { useAsyncStorage } from "@react-native-async-storage/async-storage";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { NavigationContainer } from "@react-navigation/native";
import React, { useEffect } from "react";
import { SafeAreaView } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import {
   updateCategoryListFromLocal,
   updateTaskListFromLocal,
} from "./Redux/actions/taskActions";
import { colors, globalStyles, log } from "./Static/Js/constants";
import DrawerContent from "./Views/General/DrawerContent";
import HomeStack from "./Views/General/HomeStack";
import Login from "./Views/Login";
import DoneTasksScreen from "./Views/TaskView/DoneTasksScreen";
import EditStack from "./Views/TaskView/EditStack";
import TaskDetailsScreen from "./Views/TaskView/TaskDetailsScreen";

export default function App() {
   const task = useSelector((state) => state.task);
   const categoryList = useSelector((state) => state.task.categoryList);
   const { getItem, setItem } = useAsyncStorage("@data");
   const dispatch = useDispatch();
   const { taskList } = task

   const saveToLocal = async (data) => {
      await setItem(JSON.stringify(data));
   };

   const getLocalData = async () => {
      const lData = await getItem();
      dispatch(updateTaskListFromLocal(JSON.parse(lData).taskList));
      dispatch(updateCategoryListFromLocal(JSON.parse(lData).categoryList));
   };

   useEffect(() => {
      getLocalData();
   }, []);

   useEffect(() => {
      saveToLocal({ taskList: taskList, categoryList: categoryList });
   }, [taskList, categoryList]);
   const Drawer = createDrawerNavigator();
   return (
      <React.Fragment>
         <SafeAreaView style={{ backgroundColor: colors.primary }}></SafeAreaView>
         <SafeAreaView style={globalStyles.droidSafeArea}>
            <NavigationContainer>
               <Drawer.Navigator
                  initialRouteName="Home"
                  drawerContent={(props) => <DrawerContent props={props} />}
               >
                  <Drawer.Screen name="Home" component={HomeStack} />
                  <Drawer.Screen name="Login" component={Login} />
                  <Drawer.Screen name="DoneTasks" component={DoneTasksScreen} />
                  <Drawer.Screen name="TaskDetail" component={TaskDetailsScreen} />
                  <Drawer.Screen name="EditTask" component={EditStack} />
               </Drawer.Navigator>
            </NavigationContainer>
         </SafeAreaView>
      </React.Fragment>
   );
}
