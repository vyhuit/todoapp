import { log } from '../../Static/Js/constants';
import { taskActionTypes } from '../constants/actions-types';
export const addNewTask = (newTask) => ({
    type: taskActionTypes.ADD_NEW_TASK,
    payload: newTask
})

export const updateTask = (task) => ({
    type: taskActionTypes.UPDATE_TASK,
    payload: task
})

export const addNewCate = (newCate) => ({
    type: taskActionTypes.ADD_NEW_CATEGORY,
    payload: newCate
})

export const setCate = (newTask) => ({
    type: taskActionTypes.SELECT_CATEGORY,
    payload: newTask
})
export const setCurrentNewTask = (cate) => ({
    type: taskActionTypes.SET_CURR_CATE,
    payload: cate
})
export const initLocalData = (data) => ({
    type: taskActionTypes.INIT_LOCAL_DATA,
    payload: data
})

export const updateTaskListFromLocal = (data) => ({
    type: taskActionTypes.UPDATE_TASKS_FROM_LOCAL,
    payload: data
})

export const updateCategoryListFromLocal = (data) => ({
    type: taskActionTypes.UPDATE_CATES_FROM_LOCAL,
    payload: data
})
export const updateChangeTaskState = (task) => ({
    type: taskActionTypes.UPDATE_TASK_STATE,
    payload: task
})