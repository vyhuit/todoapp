import { combineReducers } from "redux";
import { taskReducer } from './taskReducers';


const reducers = combineReducers({
   task: taskReducer
})

export default reducers;