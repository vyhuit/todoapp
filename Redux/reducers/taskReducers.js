import { taskActionTypes } from './../constants/actions-types';
import { v4 as uuidv4 } from 'uuid';
import moment from 'moment';
const initialState = {
   // taskList: [{
   //    id: 1,
   //    taskName: "Meet Joshoep",
   //    description: "Have to meet him because I want to show him my latest app design in person\nAlso need to ask for advice on these",
   //    category: {
   //       id: 1,
   //       value: "friend",
   //       label: "Friend"
   //    },
   //    dateTime: "07 Aug, 2021 | 04:00 PM",
   //    notification: "hour",
   //    completed: false,
   //    priority: "third",
   //    important: true,
   // },
   // {
   //    id: 2,
   //    taskName: "Have sex",
   //    description: "Have to meet him because I want to show him my latest app design in person\nAlso need to ask for advice on these",
   //    category: {
   //       id: 1,
   //       value: "friend",
   //       label: "Friend"
   //    },
   //    dateTime: "04 Aug, 2021 | 05:00 PM",
   //    notification: "hour",
   //    completed: false,
   //    priority: "second",
   //    important: false,
   // },
   // {
   //    id: 5,
   //    taskName: "Todo task",
   //    description: "Have to meet him because I want to show him my latest app design in person\nAlso need to ask for advice on these",
   //    category: {
   //       id: 1,
   //       value: "friend",
   //       label: "Friend"
   //    },
   //    dateTime: "04 Aug, 2021 | 06:00 PM",
   //    notification: "hour",
   //    completed: false,
   //    priority: "first",
   //    important: false,
   // },
   // {
   //    id: 3,
   //    taskName: "Sao lại đặt tên là Phong code",
   //    description: "Have to meet him because I want to show him my latest app design in person\nAlso need to ask for advice on these",
   //    category: {
   //       id: 1,
   //       value: "friend",
   //       label: "Friend"
   //    },
   //    dateTime: "10 Aug, 2021 | 05:00 PM",
   //    notification: "hour",
   //    completed: true,
   //    priority: "second",
   //    important: false,
   // },
   // {
   //    id: 4,
   //    taskName: "Phong ",
   //    description: "Have to meet him because I want to show him my latest app design in person\nAlso need to ask for advice on these",
   //    category: {
   //       id: 1,
   //       value: "friend",
   //       label: "Friend"
   //    },
   //    dateTime: "12 Aug, 2021 | 05:00 PM",
   //    notification: "hour",
   //    completed: false,
   //    priority: "third",
   //    important: false,
   // }
   // ],
   // category: {
   //    id: 0,
   //    label: "Select Category",
   //    value: null
   // },
   // categoryList: [{
   //    id: "1", // acts as primary key, should be unique and non-empty string
   //    label: "Homework",
   //    value: "homework",
   // },
   // {
   //    id: "2",
   //    label: "Housework",
   //    value: "housework",
   // },
   // {
   //    id: "3",
   //    label: "Sex",
   //    value: "sex",
   // },
   // {
   //    id: "4",
   //    label: "Friend",
   //    value: "friend",
   // },
   // ],
   // firstDayOfWeek: moment().weekday(1).format("YYYY-MM-DD"),
   // lastDayOfWeek: moment().weekday(7).format("YYYY-MM-DD"),
   // firstDayOfMonth: moment().startOf('month').format("YYYY-MM-DD"),
   // lastDayOfMonth: moment().endOf('month').format("YYYY-MM-DD"),
   taskList: [
      {
         id: 1,
         cateId: 1,
         isDone: false,
         deadline: new Date(),
         description: "Phong",
         isLater: false,
         isStar: false,
         notificationType: 1,
         priority: 1,
         name: "Task ngay 13",
         updateDeadline: null
      }
   ],
   currentTaskTemplate: {
      id: null,
      name: "",
      priority: 1,
      notificationType: 1,
      description: "",
      deadline: null,
      cateId: null,
      isLater: false,
      isStar: false,
      updateDeadline: null,
      isDone: false,
   },
   cateId: null,
   categoryList: [{
      value: 1,
      label: "Dating"
   },{
      value: 2,
      label: "Eating"
   }],
   firstDayOfWeek: moment().weekday(1).format("YYYY-MM-DD"),
   lastDayOfWeek: moment().weekday(7).format("YYYY-MM-DD"),
   firstDayOfMonth: moment().startOf('month').format("YYYY-MM-DD"),
   lastDayOfMonth: moment().endOf('month').format("YYYY-MM-DD"),
}

export const taskReducer = (state = initialState, { type, payload }) => {
   switch (type) {
      case taskActionTypes.ADD_NEW_TASK:
         {
            return {
               ...state,
               taskList: [...state.taskList, { ...payload, id: uuidv4() }],
               currentTaskTemplate: {
                  id: null,
                  name: "",
                  priority: 1,
                  notificationType: 1,
                  description: "",
                  deadline: null,
                  cateId: null,
                  isLater: false,
                  isStar: false,
                  updateDeadline: null,
                  isDone: false,
               },
            }
         }

      case taskActionTypes.INIT_LOCAL_DATA:
         return {
            ...state,
            taskList: payload.taskList,
            categoryList: payload.categoryList
         };
      case taskActionTypes.UPDATE_CATES_FROM_LOCAL:
         {
            return {
               ...state,
               categoryList: payload
            };
         }
      case taskActionTypes.UPDATE_TASKS_FROM_LOCAL:
         {
            return {
               ...state,
               taskList: payload,
            };
         }
      case taskActionTypes.UPDATE_TASK:
         {
            const index = state.taskList.findIndex(item => item.id === payload.id);
            if (index >= 0) {
               const temp = [...state.taskList];
               temp[index] = payload;
               return {
                  ...state,
                  taskList: temp
               }
            }
            return state;
         }
      case taskActionTypes.SET_CURR_CATE:
         return {
            ...state,
            currentTaskTemplate: { ...payload }
         }

      // case taskActionTypes.SELECT_CATEGORY:
      //    {
      //       // console.warn("payload", payload)
      //       // return { ...state }
      //       return { ...state, category: { ...payload } }
      //    }
      case taskActionTypes.ADD_NEW_CATEGORY:
         {
            return { ...state, category: { ...payload }, categoryList: [...state.categoryList, payload] }
            // return {...state}
         }
      case 'CHANGE_WEEKLY':
         return {
            ...state,
            firstDayOfWeek: payload.firstDay,
            lastDayOfWeek: payload.lastDay
         }
      case 'CHANGE_MONTHLY':
         return {
            ...state,
            firstDayOfMonth: payload.firstDay,
            lastDayOfMonth: payload.lastDay
         }
      case 'SET_TASK_IMPORTANT':
         const inTask = state.taskList.find(t => t.id === payload.id);
         return {
            ...state,
            taskList: inTask ? state.taskList.map(task => task.id === payload.id ? { ...task, important: payload.important } : task) : [...state.taskList]
         }
      case taskActionTypes.SELECT_CATEGORY:
         return { ...state, cateId: payload }
      case taskActionTypes.ADD_NEW_CATEGORY:
         return {
            ...state,
            currentTaskTemplate: { ...state.currentTaskTemplate, cateId: payload.value },
            categoryList: [...state.categoryList, payload]
         }
      default:
         return state;
   }
}