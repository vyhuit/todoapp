import React from "react";
import { View, Text } from "react-native";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";

function Home() {
  return (
    <View>
      <Text>I am in Home.js</Text>
    </View>
  );
}

function AboutMe() {
  return (
    <View>
      <Text>I am in AboutMe.js</Text>
    </View>
  );
}

const Tab = createMaterialTopTabNavigator();

function TabNavigation() {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="AboutMe" component={AboutMe} />
    </Tab.Navigator>
  );
}

function MainMain() {
  const navigationOptions = {
    header: null,
  };

  return (
    <View style={{ flex: 1 }}>
      <View
        style={{
          flex: 1,
          backgroundColor: "lightblue",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Text>I am in Main.js</Text>
      </View>

      <View style={{ flex: 1, backgroundColor: "lightgreen" }}>
        <TabNavigation />
      </View>
    </View>
  );
}

const Stact = createStackNavigator();

function StackNav() {
  return (
    <Stact.Navigator initialRouteName="Settings">
      <Stact.Screen
        name="Main"
        component={MainMain}
        //   options={navOptionHandler}
      />
    </Stact.Navigator>
  );
}

export default function Test() {
  return (
    <NavigationContainer>
      <StackNav />
    </NavigationContainer>
  );
}
