import React from 'react'
import Svg, { Line } from 'react-native-svg'

const PlusSvg = () => {
   return (
      <Svg height="20" width="20" viewBox="0 0 100 100">
         <Line x1="50" y1="0" x2="50" y2="100" strokeWidth="10" stroke="white" />
         <Line x1="0" y1="50" x2="100" y2="50" strokeWidth="10" stroke="white" />
      </Svg>
   )
}

export default PlusSvg
