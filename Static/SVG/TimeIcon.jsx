import React from "react";
import Svg, { Path } from "react-native-svg";

const TimeIcon = () => {
  return (
    <Svg
      id="time"
      xmlns="http://www.w3.org/2000/svg"
      width="27"
      height="27"
      viewBox="0 0 62 62"
    >
      <Path
        id="Path_47"
        data-name="Path 47"
        d="M31,0A31,31,0,1,0,62,31,31.035,31.035,0,0,0,31,0Zm0,59.933A28.933,28.933,0,1,1,59.933,31,28.966,28.966,0,0,1,31,59.933Z"
        fill="#fff"
      />
      <Path
        id="Path_48"
        data-name="Path 48"
        d="M30.567,6a1.033,1.033,0,0,0-1.033,1.033V30.8h-15.5a1.033,1.033,0,1,0,0,2.067H30.567A1.033,1.033,0,0,0,31.6,31.833V7.033A1.033,1.033,0,0,0,30.567,6Z"
        transform="translate(0.433 0.2)"
        fill="#fff"
      />
    </Svg>
  );
};

export default TimeIcon;
