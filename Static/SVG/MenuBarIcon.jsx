import React from "react";
import Svg, { Line, Path, G } from "react-native-svg";

const MenuBarIcon = () => {
  return (
    <Svg
      width="30"
      height="30"
    >
      <G id="menubar" transform="translate(-43 -123)">
        <Line
          id="Line_2"
          data-name="Line 2"
          x2="24"
          transform="translate(48 130)"
          fill="none"
          stroke="#ffffff"
          strokeLinecap="round"
          strokeWidth="3"
        />
        <Line
          id="Line_3"
          data-name="Line 3"
          x2="18"
          transform="translate(48 138)"
          fill="none"
          stroke="#ffffff"
          strokeLinecap="round"
          strokeWidth="3"
        />
        <Line
          id="Line_4"
          data-name="Line 4"
          x2="12"
          transform="translate(48 146)"
          fill="none"
          stroke="#ffffff"
          strokeLinecap="round"
          strokeWidth="3"
        />
      </G>
    </Svg>
  );
};

export default MenuBarIcon;
