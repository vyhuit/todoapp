import React from 'react'
import Svg, { Line, Path } from 'react-native-svg'

const ArrowLeftSvg = () => {
   return (
      <Svg height="20" width="20" viewBox="0 0 100 100">
         <Line x1="0" y1="50" x2="40" y2="10" strokeWidth="10" stroke="white" />
         <Line x1="0" y1="50" x2="100" y2="50" strokeWidth="10" stroke="white" />
         <Line x1="0" y1="50" x2="40" y2="90" strokeWidth="10" stroke="white" />
      </Svg>
   )
}

export default ArrowLeftSvg
