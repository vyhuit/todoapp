import moment from "moment";
import { StyleSheet, Platform } from "react-native";

const colors = {
    primary: "#7646FF",
    secondary: "#6035D0",
    blackText: "#172735",
    lightBlackText: "#717580",
    greenBold: "#4CCB41",
    green: "#4CD964",
    yellow: "#FFEB3D",
    priority1: "#FC5565",
    priority2: "#FA9B4A",
    priority3: "#58BBF7",
    priority4: "#4CCB41",
    borderGrayColor: "#9A9A9A",
    borderGrayBoldColor: "#707070",
    background: "#EFEFEF",
    white: "#fff",
    black: "#000000cf",
    defaultBackground: "#F6F6F8",
    textPrimary: "#5030A6",
    lightGray: "#d9deec47",
    blackLabel: "#172735",
    lightBlackLabel: "#53626F",
};

const globalStyles = StyleSheet.create({
    droidSafeArea: {
        flex: 1,
        backgroundColor: colors.defaultBackground,
        paddingTop: Platform.OS === "android" ? 25 : 0,
    },
});

const log = (...data) => {
    console.log("================================================");
    console.log(...data);
    console.log("================================================");
}

const compareEqualDay = (a, b) => moment(a).get("year") == moment(b).get("year") &&
    moment(a).get("month") == moment(b).get("month") &&
    moment(a).get("date") == moment(b).get("date");
export { colors, globalStyles, log, compareEqualDay };