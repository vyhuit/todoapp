import moment from "moment";

export const formatDate = (date) => {
  moment.locale("en");
  const res = moment(date).format("ll");
  return res;
};
