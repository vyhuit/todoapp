import { createDrawerNavigator } from "@react-navigation/drawer";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import * as React from "react";
import {
  Image,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { colors, globalStyles } from "./Static/Js/constants";
import NotificationScreen from "./Views/NotificationScreen";
function CustomHeader({ navigation }) {
  return (
    <View
      style={{
        flexDirection: "row",
        height: 50,
        backgroundColor: colors.white,
      }}
    >
      <View
        style={{
          flex: 1,
          justifyContent: "center",
        }}
      >
        <TouchableOpacity onPress={() => navigation.openDrawer()}>
          <Image
            style={{ width: 30, height: 30, marginLeft: 5 }}
            source={require("./Static/Images/icons/meat.png")}
            resizeMode="contain"
          />
        </TouchableOpacity>
      </View>
      <View
        style={{
          flex: 1.5,
          justifyContent: "center",
        }}
      >
        <Text style={{ textAlign: "center" }}>Title</Text>
      </View>
      <View
        style={{
          flex: 1,
          justifyContent: "center",
        }}
      ></View>
    </View>
  );
}

function HomeScreen({ navigation }) {
  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <Text>Home!</Text>
      <TouchableOpacity
        style={{ marginTop: 20 }}
        onPress={() => navigation.navigate("HomeDetail")}
      >
        <Text>Go Home Detail</Text>
      </TouchableOpacity>
      <CustomHeader navigation={navigation} />
    </View>
  );
}
function HomeScreenDetail() {
  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <Text>Home Detail!</Text>
    </View>
  );
}

function SettingsScreen({ navigation }) {
  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <Text>Settings!</Text>
      <TouchableOpacity
        style={{ marginTop: 20 }}
        onPress={() => navigation.navigate("SettingsDetail")}
      >
        <Text>Go Settings Detail</Text>
      </TouchableOpacity>
    </View>
  );
}

function SettingsScreenDetail() {
  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <Text>Settings Detail!</Text>
    </View>
  );
}
const Tab = createMaterialTopTabNavigator();

const navOptionHandler = () => ({
  headerShown: false,
});
const StackHome = createStackNavigator();

function HomeStack() {
  return (
    <StackHome.Navigator initialRouteName="Home">
      <StackHome.Screen
        name="Home"
        component={HomeScreen}
        options={navOptionHandler}
      />
      <StackHome.Screen
        name="HomeDetail"
        component={HomeScreenDetail}
        options={navOptionHandler}
      />
    </StackHome.Navigator>
  );
}

const StackSetting = createStackNavigator();

function SettingsStack() {
  return (
    <StackSetting.Navigator initialRouteName="Settings">
      <StackSetting.Screen
        name="Settings"
        component={SettingsScreen}
        options={navOptionHandler}
      />
      <StackSetting.Screen
        name="SettingsDetail"
        component={SettingsScreenDetail}
        options={navOptionHandler}
      />
    </StackSetting.Navigator>
  );
}

const Drawer = createDrawerNavigator();

function CustomDrawerContent(props) {
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "red" }}>
      <ScrollView>
        <TouchableOpacity
          style={{ marginTop: 20 }}
          onPress={() => props.navigation.navigate("MenuTab")}
        >
          <Text>Menu Tab</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{ marginTop: 20 }}
          onPress={() => props.navigation.navigate("Notifications")}
        >
          <Text>Notification</Text>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  );
}

// function NotificationsScreen({ navigation }) {
//   return (
//     <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
//       <Button onPress={() => navigation.goBack()} title="Go back home" />
//       <CustomHeader navigation={navigation} />
//       <Text>xinchao taat ca moi nguoi</Text>
//     </View>
//   );
// }

function TabNavigation({ navigation }) {
  return (
    <React.Fragment>
      <CustomHeader navigation={navigation} />
      <Tab.Navigator>
        <Tab.Screen name="Home" component={HomeStack} />
        <Tab.Screen name="Settings" component={SettingsStack} />
      </Tab.Navigator>
    </React.Fragment>
  );
}

export default function TodoApp() {
  return (
    <SafeAreaView style={globalStyles.droidSafeArea}>
      <NavigationContainer>
        <Drawer.Navigator
          initialRouteName="MenuTab"
          drawerContent={(props) => CustomDrawerContent(props)}
        >
          <Drawer.Screen name="MenuTab" component={TabNavigation} />
          <Drawer.Screen name="Notifications" component={NotificationScreen} />
        </Drawer.Navigator>
      </NavigationContainer>
    </SafeAreaView>
  );
}
