import React from 'react';
import { AppRegistry } from "react-native";
import "react-native-gesture-handler";
import { Provider as PaperProvider } from 'react-native-paper';
import { Provider } from "react-redux";
import App from "./App";
import store from "./Redux/store";

// registerRootComponent calls AppRegistry.registerComponent('main', () => App);
// It also ensures that whether you load the app in Expo Go or in a native build,
// the environment is set up appropriately

const RNRedux = () => (
   <PaperProvider>
      <Provider store={store}>
         <App />
      </Provider>
   </PaperProvider>
)

AppRegistry.registerComponent('main', () => RNRedux);
