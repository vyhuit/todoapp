import React from "react";
import { View, Text } from "react-native";
import { colors } from "../Static/Js/constants";
import HeaderNavigation from "./General/HeaderNavigation";

const NotificationScreen = ({ navigation }) => {
   return (
      <View>
         <HeaderNavigation
            showNotiBell={false}
            title="Thông báo"
            isBackable="true"
            showSearchBar={false}
            onPressButton={() => navigation.navigate("Home")}
         />
         <View style={{ flex: 1, backgroundColor: colors.redPriority }}>
            <Text>Đây là trang thông báo</Text>
         </View>
      </View>
   );
};

export default NotificationScreen;
