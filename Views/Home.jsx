import React from "react";
import HeaderNavigation from "./General/HeaderNavigation";
import TabNavigation from "./General/TabNavigation";

export default function Home({ navigation }) {
  return (
    <React.Fragment>
      <HeaderNavigation onPressButton={() => navigation.openDrawer()} />
      <TabNavigation navigation={navigation} />
    </React.Fragment>
  );
}
