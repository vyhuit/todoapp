import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TextInput,
  Image,
} from "react-native";
import { Button } from "react-native-paper";
import { generalStyles, colors } from "../Static/Js/constants";

export default function Login({ navigation }) {
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");

  const onLoginPress = () => {
    if (userName.toLocaleLowerCase() == "admin" && password == "123123")
      navigation.navigate("Home");
    else alert("Invalid Account. \nUse admin/123123 instead!");
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.login}>
        <Text style={styles.loginTitle}>Login</Text>
        <TextInput
          onChangeText={setUserName}
          style={styles.loginInput}
          placeholder="User Name"
        />
        <TextInput
          onChangeText={setPassword}
          style={styles.loginInput}
          placeholder="Password"
        />
        <Button
          mode="contained"
          style={styles.loginBtn}
          onLongPress={() => navigation.navigate("Home")}
          onPress={onLoginPress}
        >
          <Text style={styles.loginTextBtn}>Login</Text>
        </Button>
        <View style={styles.logoWrapper}>
          <Image
            resizeMode="contain"
            style={styles.loginLogo}
            source={require("../Static/Images/general/logo.png")}
          />
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
  },
  loginTitle: {
    width: "100%",
    textAlign: "center",
    fontSize: 40,
    color: colors.black,
    fontWeight: "bold",
    marginBottom: 50,
  },
  login: {
    paddingHorizontal: 50,
    paddingVertical: 200,
    width: "100%",
    height: "100%",
  },
  loginInput: {
    height: 50,
    borderColor: colors.borderGrayColor,
    borderRadius: 5,
    borderWidth: 0.5,
    paddingHorizontal: 10,
    width: "100%",
    marginBottom: 5,
  },
  loginBtn: {
    backgroundColor: colors.primary,
    marginTop: 15,
  },
  loginTextBtn: {
    fontSize: 18,
  },
  loginLogo: {
    width: "70%",
    justifyContent: "center",
    marginTop: 30,
    flex: 1,
  },
  logoWrapper: {
    marginTop: 30,
    flex: 1,
    height: "auto",
    display: "flex",
    alignItems: "center",
  },
});
