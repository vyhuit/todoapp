import React, { useEffect, useState } from "react";
import { View, Text } from "react-native";
import { RadioButton } from "react-native-paper";
import { colors, log } from "../../Static/Js/constants";
import RadioGroup from "react-native-radio-buttons-group";

const Priority = ({ priority, setPriority }) => {
  const radioButtonsData = [
    {
      id: "1",
      key: "1",
      label: "1",
      value: 1,
      color: colors.priority1,
      selected: priority == 1 ? true : false,
    },
    {
      id: "2",
      key: "2",
      value: 2,
      label: "2",
      color: colors.priority2,
      selected: priority == 2 ? true : false,
    },
    {
      id: "3",
      key: "3",
      value: 3,
      label: "3",
      color: colors.priority3,
      selected: priority == 3 ? true : false,
    },
    {
      id: "4",
      key: "4",
      value: 4,
      label: "4",
      color: colors.priority4,
      selected: priority == 4 ? true : false,
    },
  ];

  const [radioButtons, setRadioButtons] = useState(radioButtonsData);

  const onPressRadioButton = (radioButtonsArray) => {
    setRadioButtons(radioButtonsArray);
    let checkItemValue = radioButtonsArray.find(
      (item) => item.selected == true
    ).value;
    setPriority(checkItemValue);
  };
  return (
    <View
      style={{
        paddingHorizontal: 8,
        height: 60,
        display: "flex",
        justifyContent: "center",
        backgroundColor: colors.white,
      }}
    >
      <RadioGroup
        radioButtons={radioButtons}
        onPress={onPressRadioButton}
        layout="row"
        value
      />
    </View>
  );
};

export default Priority;
