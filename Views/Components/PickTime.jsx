import React, { useEffect, useState } from 'react';
import { View, Button, Platform, TouchableOpacity, Text } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import { colors } from '../../Static/Js/constants';
import moment from 'moment';
let first = true;
const PickTime = ({ dateTimex, setDateTime }) => {
   const [date, setDate] = useState(new Date());
   const [mode, setMode] = useState('date');
   const [show, setShow] = useState(false);
   const [time, setTime] = useState(new Date());
   const [first, setFirst] = useState(true);
   //...
   useEffect(() => {
      if (!first) {
         console.warn(dateTimex)
         const dateTime = formatDate(date, time);
         setDateTime(dateTime);
      }
   }, [])


   const onChange = (event, selectedValue) => {
      setFirst(false)
      setShow(Platform.OS === 'ios');
      if (mode == 'date') {
         const currentDate = selectedValue || new Date();
         setDate(currentDate);
         setMode('time');
         setShow(Platform.OS !== 'ios'); // to show the picker again in time mode
      } else {
         const selectedTime = selectedValue || new Date();
         setTime(selectedTime);
         setShow(Platform.OS === 'ios');
         setMode('date');
      }

   };

   const formatDate = (date, time) => {
      // console.warn("newDate", now)
      return `${moment(date).format('MMM Do, YYYY')} | ${moment(time).format('h:mm A')}`;
      // return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()} | ${time.getHours()}:${time.getMinutes()}`;
   }

   useEffect(() => {
      if (!first) {
         console.warn(dateTimex)
         const dateTime = formatDate(date, time);
         setDateTime(dateTime);
      }
      // const dateTime = formatDate(date, time);
      // setDateTime(dateTime);
   }, [date, time])

   return (
      <View>
         <TouchableOpacity onPress={() => setShow(true)}>
            <Text style={{ paddingVertical: 15, paddingHorizontal: 10, fontSize: 16, backgroundColor: colors.white, borderBottomColor: colors.borderGrayColor, borderBottomWidth: 1 }}>{(first && dateTimex) ? dateTimex : formatDate(date, time)}</Text>
         </TouchableOpacity>
         {show && (
            <DateTimePicker
               testID="dateTimePicker"
               timeZoneOffsetInMinutes={0}
               value={date}
               mode={mode}
               is24Hour={true}
               locale="vi"
               display="default"
               onChange={onChange}
            />
         )}
      </View>
   );
};

export default PickTime;
