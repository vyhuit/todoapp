import { useNavigation } from "@react-navigation/native";
import React, { useEffect, useState } from "react";
import {
  Alert, Keyboard,
  ScrollView,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import { RadioButton, TextInput } from "react-native-paper";
import { useDispatch, useSelector } from "react-redux";
import {
  addNewCate, setCurrentNewTask
} from "../../Redux/actions/taskActions";
import { colors, log } from "../../Static/Js/constants";
import { HRLine } from "../TaskView/TaskList";
import HeaderNavigation from "./../General/HeaderNavigation";
import { v4 as uuidv4 } from 'uuid';

const SelectCategory = ({ route }) => {
  // const { setCategory } = route.params;
  const [newCateLabel, setNewCateLabel] = useState("");
  const categoryList = useSelector((state) => state.task.categoryList);
  const currentTaskTemplate = useSelector(
    (state) => state.task.currentTaskTemplate
  );
  const dispatch = useDispatch();

  const addNewCategory = async () => {
    Keyboard.dismiss();
    if (newCateLabel != "") {
      const newOption = {
        label: newCateLabel,
        value: uuidv4(),
      };
      dispatch(addNewCate(newOption));
      setNewCateLabel("");
    } else alert("Please enter category name");
  };

  const onChangeRadioButton = async (value) => {
    Keyboard.dismiss();
    await dispatch(
      setCurrentNewTask({ ...currentTaskTemplate, cateId: value })
    );
  };

  const navigation = useNavigation();

  const selectCategory = async () => {
    if (currentTaskTemplate.cateId) navigation.goBack();
    else
      Alert.alert("Select Category", "You haven't selected a category", [
        { text: "OK" },
      ]);
  };
  log("Select",categoryList)
  return (
    <View>
      <HeaderNavigation
        title="Select Category"
        isBackable={true}
        showSearchBar={false}
        showNotiBell={false}
        showNewTask={false}
        onPressButton={selectCategory}
      />
      <View
        style={{
          flexDirection: "column",
          height: 55,
          marginHorizontal: 18,
          marginTop: 15,
        }}
      >
        <TextInput
          style={{
            backgroundColor: colors.white,
            flex: 1,
            height: 55,
            borderRadius: 10,
            // borderbottomWidth:0,
            // borderBottomColor:"transparent",
          }}
          placeholder="Category Name"
          onChangeText={setNewCateLabel}
          value={newCateLabel}
        />
      </View>
      <View style={{ alignItems: "center", marginHorizontal: 18 }}>
        <TouchableOpacity
          style={{
            width: "100%",
            paddingHorizontal: 5,
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: colors.secondary,
            borderRadius: 5,
            height: 55,
            marginVertical: 5,
          }}
          onPress={addNewCategory}
        >
          <Text
            style={{ color: colors.white, fontSize: 15, fontWeight: "bold" }}
          >
            ADD TO LIST
          </Text>
        </TouchableOpacity>
      </View>
      <Text
        style={{
          fontWeight: "bold",
          color: colors.blackLabel,
          paddingHorizontal: 18,
          paddingVertical: 10,
          fontSize: 16,
        }}
      >
        Category List
      </Text>
      <ScrollView
        keyboardShouldPersistTaps="handled"
        style={{ height: "100%" }}
      >
        <RadioButton.Group
          onValueChange={(value) => onChangeRadioButton(value)}
          value={currentTaskTemplate.cateId}
        >
          {categoryList.length > 0 ? (
            categoryList.map((item) => (
              <View key={item.value}>
                <RadioButton.Item
                  labelStyle={{ position: "absolute", left: 70 }}
                  color={colors.primary}
                  position="leading"
                  label={item.label}
                  value={item.value}
                  style={{ backgroundColor: colors.white }}
                />
                <HRLine opacity={0.2} />
              </View>
            ))
          ) : (
            <Text
              style={{
                textAlign: "center",
                paddingVertical: 20,
                color: colors.lightBlackText,
              }}
            >
              No category. Please add a new category.
            </Text>
          )}
        </RadioButton.Group>
      </ScrollView>
    </View>
  );
};

export default SelectCategory;
