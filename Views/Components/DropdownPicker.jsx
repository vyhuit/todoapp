import React, { useState } from "react";
import DropDownPicker from "react-native-dropdown-picker";
import { colors } from "../../Static/Js/constants";

const DropdownPicker = () => {
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const [items, setItems] = useState([
    { label: "Apple", value: "apple" },
    { label: "Banana", value: "banana" },
    { label: "Add a new category", value: "addNew" },
  ]);
  return (
    <DropDownPicker
      open={open}
      value={value}
      items={items}
      setOpen={setOpen}
      setValue={setValue}
      setItems={setItems}
      onChangeValue={(value) => {
        console.log(value);
      }}
      style={{
        backgroundColor: colors.white,
        borderRadius: 0,
        borderTopColor: colors.white,
        borderBottomColor: colors.borderGrayColor,
        height: 60,
      }}
      dropDownContainerStyle={{
        backgroundColor: "red",
        borderRadius: 0,
      }}
      placeholder="Category"
      placeholderStyle={{
        color: colors.borderGrayColor,
        fontSize: 16,
      }}
      searchable={true}
    />
  );
};

export default DropdownPicker;
