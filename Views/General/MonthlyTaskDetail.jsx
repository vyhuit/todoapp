import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { View } from 'react-native';
import { useSelector } from 'react-redux';
import { colors } from '../../Static/Js/constants';
import TaskList from '../TaskView/TaskList';
const today = moment().format("YYYY-MM-DD");
const MonthlyTaskDetail = ({ }) => {
   const [tasksOfDay, setTasksOfDay] = useState([]);
   const task = useSelector(state => state.task);
   const { firstDayOfMonth, lastDayOfMonth, taskList } = task;

   useEffect(() => {
      let daysInmonth = moment(firstDayOfMonth).daysInMonth();
      let taskArray = [];
      if (firstDayOfMonth === lastDayOfMonth) {
         let day = moment(firstDayOfMonth).format("YYYY-MM-DD")
         let dayObj = {
            'day': day === today ? 'today' : day,
            'data': []
         }
         let todayTasks = taskList.filter(t => {
            let d = moment(t.deadline).format("YYYY-MM-DD");
            if (d === day) {
               return true;
            }
            return false;
         })
         dayObj.data = todayTasks;
         dayObj.data.length !== 0 && taskArray.push(dayObj);
      } else {
         for (let i = 0; i < daysInmonth; i++) {
            let day = moment(firstDayOfMonth).add(i, 'days').format("YYYY-MM-DD");
            let dayObj = {
               'day': day === today ? 'today' : day,
               'data': []
            }
            let todayTasks = taskList.filter(t => {
               let d = moment(t.deadline).format("YYYY-MM-DD");
               if (d === day) {
                  return true;
               }
               return false;
            })
            dayObj.data = todayTasks;
            dayObj.data.length !== 0 && taskArray.push(dayObj);
         }
         taskArray = taskArray.sort(function (x, y) { return x.day == 'today' ? -1 : y.day == 'today' ? 1 : 0; });
      }
      setTasksOfDay(taskArray);

   }, [firstDayOfMonth, lastDayOfMonth, taskList])

   return (
      <View style={{ flex: 1, backgroundColor: colors.defaultBackground, justifyContent: "center" }}>
         <TaskList data={tasksOfDay} />
      </View>
   );
}

export default MonthlyTaskDetail;
