import React from "react";
import {
   Image,
   SafeAreaView,
   ScrollView,
   StyleSheet,
   Text,
   TouchableOpacity,
   View,
} from "react-native";
import { colors } from "../../Static/Js/constants";
const actions = [
   {
      id: 1,
      iconName: require("../../Static/Images/icons/plus.png"),
      text: "New Task",
      navigateTo: "NewTask",
   },
   {
      id: 2,
      iconName: require("../../Static/Images/icons/star.png"),
      text: "Important",
      navigateTo: "Home",
   },
   {
      id: 3,
      iconName: require("../../Static/Images/icons/checkmark.png"),
      text: "Done",
      navigateTo: "DoneTasks",
   },
   {
      id: 4,
      iconName: require("../../Static/Images/icons/clock.png"),
      text: "Later",
      navigateTo: "Home",
   },
   {
      id: 5,
      iconName: require("../../Static/Images/icons/tag.png"),
      text: "Category",
      navigateTo: "Home",
   },
   // {
   //    id: 8,
   //    iconName: require("../../Static/Images/icons/tag.png"),
   //    text: "Edit Task",
   //    navigateTo: "NewTask",
   //    params: { id: 1 }
   // },
   // {
   //    id: 7,
   //    iconName: require("../../Static/Images/icons/settings.png"),
   //    text: "Detail Task",
   //    navigateTo: "TaskDetail"
   // },
   // {
   //    id: 6,
   //    iconName: require("../../Static/Images/icons/settings.png"),
   //    text: "Settings",
   //    navigateTo: "Home",
   // },

];

const logout = (props) => {
   props.navigation.navigate("Login");
};
const DrawerContent = ({ props }) => {
   return (
      <SafeAreaView style={{ flex: 1, backgroundColor: colors.white }}>
         <View
            style={{
               height: 200,
               backgroundColor: colors.primary,
               justifyContent: "space-evenly",
               alignItems: "center",
            }}
         >
            <Image
               // size={100}
               style={{
                  width: 120,
                  height: 120,
               }}
               source={require("../../Static/Images/general/defaultAvatar.png")}
            />
            <Text
               style={{
                  color: colors.white,
                  fontWeight: "bold",
                  fontSize: 20,
                  textTransform: "capitalize",
               }}
            >
               Administrator
            </Text>
         </View>
         <ScrollView>
            <View
               style={{
                  paddingHorizontal: 20,
               }}
            >
               {actions.map((action) => (
                  <TouchableOpacity
                     key={action.id}
                     style={{
                        // marginTop: 15,
                        flexDirection: "row",
                        alignItems: "center",
                        height: 60,
                     }}
                     onPress={() => props.navigation.navigate(action.navigateTo, action.params ? action.params : null)}
                  >
                     <Image
                        style={{ width: 20, height: 20, marginLeft: 5 }}
                        source={action.iconName}
                        resizeMode="contain"
                     />
                     <Text style={styles.labelDrawer}>{action.text}</Text>
                  </TouchableOpacity>
               ))}

               <TouchableOpacity
                  style={{
                     flexDirection: "row",
                     alignItems: "center",
                     height: 60,
                  }}
                  onPress={() => logout(props)}
               >
                  <Image
                     style={{ width: 20, height: 20, marginLeft: 5 }}
                     source={require("../../Static/Images/icons/logout.png")}
                     resizeMode="contain"
                  />
                  <Text style={styles.labelDrawer}>Logout</Text>
               </TouchableOpacity>
            </View>
         </ScrollView>
      </SafeAreaView>
   );
};

const styles = StyleSheet.create({
   labelDrawer: {
      fontSize: 16,
      paddingLeft: 15,
      //  paddingBottom: 2,
   },
});

export default DrawerContent;
