import { useNavigation } from "@react-navigation/native";
import React from "react";
import {
  SafeAreaView,
  StyleSheet,
  Text, TouchableOpacity,
  View
} from "react-native";
import { Badge } from "react-native-elements";
import { Appbar, TextInput } from "react-native-paper";
import { colors } from "../../Static/Js/constants";
import ArrowLeftSvg from "../../Static/SVG/ArrowLeftSvg";
import MenuBarIcon from "../../Static/SVG/MenuBarIcon";
import PlusSvg from "../../Static/SVG/PlusSvg";

export default function HeaderNavigation({
  showNewTask = true,
  showSearchBar = true,
  showNotiBell = true,
  isBackable = false,
  title = "Todo App",
  onPressButton,
}) {
  const navigatetion = useNavigation();
  return (
    <SafeAreaView style={styles.container}>
      <View style={{}}>
        <Appbar
          style={[
            styles.topAppBar,
            { paddingHorizontal: 18, alignItems: "center" },
          ]}
        >
          <View style={styles.appbarLayout}>
            <TouchableOpacity
              style={{ marginRight: 10 }}
              onPress={onPressButton}
            >
              {isBackable ? <ArrowLeftSvg /> : <MenuBarIcon />}
            </TouchableOpacity>
            <Text style={styles.appBarTitle}>{title}</Text>
          </View>
          <View style={[styles.appbarLayout, { alignItems: "center" }]}>
            {/* {showNotiBell && (
              <View>
                <Appbar.Action
                  style={{ color: "red" }}
                  color="white"
                  icon="bell"
                  onPress={() => navigatetion.navigate("Notifications")}
                />
                <Badge
                  status="error"
                  containerStyle={{ position: "absolute", top: 14, right: 14 }}
                />
              </View>
            )} */}
            {showNewTask && (
              <TouchableOpacity
                onPress={() => navigatetion.navigate("NewTask")}
              >
                <PlusSvg />
              </TouchableOpacity>
            )}
          </View>
        </Appbar>
        <View style={{ backgroundColor: colors.primary, paddingBottom: 10 }}>
          {showSearchBar && (
            <TextInput
              style={styles.searchInput}
              placeholder="Search Task ..."
              returnKeyType="search"
            />
          )}
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.primary,
    //  height: "100%",
    //  minHeight: "20%",
  },
  appBarTitle: {
    fontSize: 22,
    fontWeight: "bold",
    color: "white",
    //  flex: 1,
  },
  appbarLayout: {
    flexDirection: "row",
    alignItems: "center",
    position: "relative",
  },
  contentArea: {
    flex: 1,
    backgroundColor: colors.yellow,
  },
  topAppBar: {
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: colors.primary,
    shadowColor: "transparent",
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0,
    shadowRadius: 0,
    height: "auto",
    paddingVertical: 5,
  },
  //   layout: {
  //     // marginTop: 100,
  //     backgroundColor: colors.green,
  //     display: "flex",
  //     flexDirection: "column",
  //     flex: 1,
  //     //  height: "100%",
  //   },
  searchInput: {
    marginVertical: 8,
    marginHorizontal: 18,
    height: 40,
    borderRadius: 5,
    borderBottomWidth: 0,
    paddingHorizontal: 10,
    backgroundColor: "white",
  },
});
