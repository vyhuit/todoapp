import {
   createStackNavigator,
   TransitionPresets,
} from "@react-navigation/stack";
import React from "react";
import Home from "../Home";
import NotificationScreen from "../NotificationScreen";
import NewTaskScreen from "../../Views/NewTaskScreen";
import DoneTasksScreen from "../TaskView/DoneTasksScreen";
import SelectCategory from "./../Components/SelectCategory";
import TaskDetailsScreen from "../TaskView/TaskDetailsScreen";

const Stack = createStackNavigator();

export default function HomeStack() {
   return (
      <Stack.Navigator
         initialRouteName="Home"
         screenOptions={{
            gestureDirection: "horizontal",
            gestureEnabled: true,
            ...TransitionPresets.SlideFromRightIOS,
         }}
      >
         <Stack.Screen name="Home" component={Home} options={navOptionHandler} />
         <Stack.Screen
            name="Notifications"
            component={NotificationScreen}
            options={navOptionHandler}
         />
         <Stack.Screen
            name="NewTask"
            component={NewTaskScreen}
            options={navOptionHandler}
         />
         <Stack.Screen
            name="Category"
            component={SelectCategory}
            options={navOptionHandler}
         />
         <Stack.Screen
            name="TaskDetail"
            component={TaskDetailsScreen}
            options={navOptionHandler}
         />
      </Stack.Navigator>
   );
}

const navOptionHandler = () => ({
   headerShown: false,
});
