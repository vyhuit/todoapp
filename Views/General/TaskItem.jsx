import moment from "moment";
import React, { useState } from "react";
import TimeIcon from "../../Static/SVG/TimeIcon";
import DoneIcon from "../../Static/SVG/DoneIcon";
import {
   Alert,
   Image,
   StyleSheet,
   Text,
   TouchableOpacity,
   View,
} from "react-native";
import { RectButton } from "react-native-gesture-handler";
import Swipeable from "react-native-gesture-handler/Swipeable";
import { useDispatch, useSelector } from "react-redux";
import { colors, log } from "../../Static/Js/constants";
import { updateTask } from "../../Redux/actions/taskActions";
import { useNavigation } from "@react-navigation/native";
const TaskItem = ({ task, showDetailClick, expandTaskClick }) => {
   const cateList = useSelector((state) => state.task.categoryList);
   const dispatch = useDispatch();
   const clickSwipeRight = () => {
      if (typeof expandTaskClick == "function") expandTaskClick(task);
      else alert("Please pass expandTaskClick to TaskItem component.");
   };
   const clickSwipeLeft = () => {
      dispatch(updateTask({ ...task, isDone: !task.isDone }));
   };
   const isImportantHandle = () => {
      dispatch(updateTask({ ...task, isStar: !task.isStar }));
   };
   const navigation = useNavigation();
   const TaskDetail = (task) => {
      navigation.navigate("TaskDetail", task )
      // if (typeof showDetailClick == "function") showDetailClick(task);
      // else alert("Please pass showDetailClick to TaskItem component.");
   };
   const getCateLabel = () => {
      if (task.cateId) {
         let item = cateList.find((item) => item.value == task.cateId);
         return item ? item.label : "No Category";
      } else return "No Category";
   };

   const renderRightActions = () => {
      return (
         <RectButton
            style={{
               backgroundColor: task.isLater
                  ? colors.lightBlackLabel
                  : colors.priority1,
               justifyContent: "center",
               flexDirection: "row",
               alignItems: "center",
               minWidth: 130,
            }}
            enabled={!task.isLater}
            onPress={clickSwipeRight}
         >
            <TimeIcon />
            <Text style={{ fontSize: 16, color: colors.white, marginLeft: 20 }}>
               {task.isLater ? "Expanded" : "Later"}
            </Text>
         </RectButton>
      );
   };
   const renderLeftActions = () => {
      return (
         <RectButton
            style={{
               backgroundColor: task.isDone ? colors.priority3 : colors.greenBold,
               justifyContent: "center",
               flexDirection: "row",
               alignItems: "center",
               minWidth: 130,
            }}
            onPress={clickSwipeLeft}
         >
            <DoneIcon />
            <Text style={{ fontSize: 16, color: colors.white, marginLeft: 20 }}>
               {task.isDone ? "Undone" : "Done"}
            </Text>
         </RectButton>
      );
   };

   const truncateLabel = (value) => {
      return value.substr(0, 25) + (value.length > 25 ? "..." : "");
   };

   return (
      <View style={styles.container}>
         <Swipeable
            renderLeftActions={renderLeftActions}
            renderRightActions={renderRightActions}
         >
            <TouchableOpacity onPress={() => TaskDetail(task)}>
               <View style={styles.paragraph}>
                  <View
                     style={{
                        flexDirection: "row",
                        flex: 1,
                        alignItems: "flex-start",
                     }}
                  >
                     <View style={styles.timeAction}>
                        <Text
                           style={{
                              fontSize: 14,
                              color: colors.black,
                              fontWeight: "bold",
                              marginBottom: 5,
                           }}
                        >
                           {moment(task.deadline).format("h:mm")}
                        </Text>
                        <Text
                           style={{
                              color: colors.lightBlackText,
                              fontWeight: "bold",
                              fontSize: 16,
                           }}
                        >
                           {moment(task.deadline).format("A")}
                        </Text>
                     </View>

                     <View style={[styles.nameAction, { marginLeft: 22 }]}>
                        <Text
                           numberOfLines={1}
                           style={{
                              fontWeight: "bold",
                              fontSize: 15,
                              color: colors.black,
                              marginBottom: 5,
                           }}
                        >
                           {truncateLabel(task.name)}
                        </Text>
                        <Text
                           style={{
                              color: colors.lightBlackLabel,
                              fontSize: 14,
                           }}
                           numberOfLines={1}
                        >
                           {truncateLabel(getCateLabel())}
                        </Text>
                     </View>
                  </View>

                  <View
                     style={[
                        styles.actionAction,
                        {
                           shadowColor: colors.lightBlackText,
                           shadowOffset: {
                              width: 0,
                              height: 2,
                           },
                           shadowOpacity: 0.4,
                           shadowRadius: 2.5,
                           elevation: 2,
                           alignItems: "center",
                           justifyContent: "space-between",
                        },
                     ]}
                  >
                     <TouchableOpacity onPress={isImportantHandle}>
                        {task.isStar ? (
                           <Image
                              source={require("../../Static/Images/icons/yellowStar.png")}
                              style={{ width: 20, height: 20 }}
                           />
                        ) : (
                           <Image
                              source={require("../../Static/Images/icons/star.png")}
                              style={{ width: 20, height: 20 }}
                           />
                        )}
                     </TouchableOpacity>
                     <View
                        style={{
                           height: 20,
                           width: 20,
                           borderRadius: 20,
                           backgroundColor: colors["priority" + task.priority],
                           shadowColor: colors.lightBlackText,
                           shadowOffset: {
                              width: 0,
                              height: 2,
                           },
                           shadowOpacity: 0.4,
                           shadowRadius: 2.5,
                           elevation: 2,
                        }}
                     ></View>
                  </View>
               </View>
            </TouchableOpacity>
         </Swipeable>
      </View>
   );
};

const styles = StyleSheet.create({
   container: {
      backgroundColor: colors.defaultBackground,
      display: "flex",
      justifyContent: "center",
      height: 60,
   },
   paragraph: {
      height: "100%",
      backgroundColor: colors.white,
      paddingHorizontal: 18,
      paddingVertical: 10,
      shadowColor: colors.borderGrayColor,
      shadowOffset: { width: 1, height: 1 },
      shadowOpacity: 0.2,
      shadowRadius: 3,
      flexDirection: "row",
      justifyContent: "space-between",
      fontSize: 18,
      borderBottomWidth: 0.5,
   },
   timeAction: {
      width: "15%",
      maxWidth: 40,
      alignItems: "center",
   },
   nameAction: {
      alignItems: "flex-start",
      alignContent: "flex-start",
   },
   actionAction: {
      width: "20%",
      flexDirection: "row",
      alignItems: "flex-end",
      maxWidth: 60,
   },
   boldText: {
      fontWeight: "bold",
      textTransform: "uppercase",
   },
   complete: {
      //  gap: "0.5rem",
      justifyContent: "center",
      color: colors.green,
      alignItems: "center",
   },
   leftAction: {
      justifyContent: "center",
      // backgroundColor: colors.green,
      flexDirection: "row",
      alignItems: "center",
   },
   textSwipe: {
      color: colors.white,
      marginStart: 10,
      fontSize: 20,
      width: 100,
      flexDirection: "row",
   },
   leftSwipe: {
      // backgroundColor: colors.lightBlackText,
      color: colors.white,
      marginStart: 10,
      fontSize: 20,
      width: 100,
      flexDirection: "row",
   },
});

export default TaskItem;
