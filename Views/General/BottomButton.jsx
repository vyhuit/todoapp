import React from 'react'
import { Keyboard, View } from 'react-native'
import { Button } from 'react-native-paper'

const BottomButton = ({ text = "button Text", onPress, }) => {
   return (
      <View>
         <Button style={{ borderRadius: 0, paddingVertical: 10 }} labelStyle={{ fontSize: 18, fontWeight: 'bold' }} mode="contained" onPress={() => {
            Keyboard.dismiss();
            onPress ? onPress() : console.log("chuaw co press");
         }}>
            {text}
         </Button>
      </View>
   )
}

export default BottomButton
