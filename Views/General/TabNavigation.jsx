import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import React from "react";
import DailyTasks from "../TaskView/DailyTasks";
import WeeklyTasks from "../TaskView/WeeklyTasks";
import { Dimensions, View, StyleSheet } from "react-native";
import MonthlyTasks from "../TaskView/MonthlyTasks";
import { colors } from "../../Static/Js/constants";
const initialLayout = { width: Dimensions.get("window").width };
const Tab = createMaterialTopTabNavigator();

const TabNavigation = () => {
  return (
    <React.Fragment>
      <Tab.Navigator
        tabBarOptions={{
          labelStyle: {
            color: colors.white,
            fontWeight: "bold",
            letterSpacing: 2,
          },
          indicatorStyle: {
            borderBottomColor: "white",
            borderBottomWidth: 4,
          },
          style: {
            backgroundColor: colors.primary,
          },
        }}
        initialLayout={initialLayout}
      //   swipeEnabled={false}
      >
        <Tab.Screen name="Daily" component={DailyTasks} />
        <Tab.Screen name="Weekly" component={WeeklyTasks} />
        <Tab.Screen name="Monthly" component={MonthlyTasks} />
      </Tab.Navigator>
    </React.Fragment>
  );
};

const styles = StyleSheet.create({});

export default TabNavigation;
