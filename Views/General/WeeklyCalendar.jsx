import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { View } from 'react-native';
import CalendarStrip from 'react-native-calendar-strip';
import { useDispatch, useSelector } from 'react-redux';
import { colors } from '../../Static/Js/constants';
const today = moment();
const WeeklyCalendar = ({ }) => {
   const taskList = useSelector(state => state.task.taskList);
   const [markedDates, setMarkedDates] = useState([]);
   const dispatch = useDispatch();

   useEffect(() => {
      let array = [];
      taskList.forEach(task => {
         array.push({
            date: moment(task.deadline).format("YYYY-MM-DD"),
            dots: [
               {
                  color: colors.greenBold,
               },
            ]
         })
      })
      setMarkedDates(array)
   }, [taskList])
   return (
      <View style={{}}>
         <CalendarStrip
            calendarAnimation={{ type: 'sequence', duration: 0 }}
            scrollable={true}
            scrollerPaging={true}
            style={{ height: 92, paddingTop: 40 }}
            calendarHeaderStyle={{ color: colors.lightBlackText, height: 40, width: "100%", backgroundColor: colors.background, alignItems: 'center', position: 'absolute', bottom: 0, paddingTop: 10 }}
            calendarColor={colors.primary}
            dateNumberStyle={{ color: 'white' }}
            dateNameStyle={{ color: 'white' }}
            highlightDateNumberStyle={{ color: colors.black, backgroundColor: 'white', width: 50, paddingBottom: 30, }}
            highlightDateNameStyle={{ color: colors.black, backgroundColor: 'white', width: 50, fontWeight: 'bold', paddingTop: 40 }}
            iconContainer={{ display: 'none' }}
            selectedDate={today}
            markedDates={markedDates}
            onWeekScrollEnd={(start, end) => {
               dispatch({
                  type: 'CHANGE_WEEKLY', payload: {
                     firstDay: start.add(1, 'days').format("YYYY-MM-DD"),
                     lastDay: end.add(1, 'days').format("YYYY-MM-DD")
                  }
               })
            }}
            onDateSelected={(date) => {
               dispatch({
                  type: 'CHANGE_WEEKLY', payload: {
                     firstDay: date.format("YYYY-MM-DD"),
                     lastDay: date.format("YYYY-MM-DD")
                  }
               })
            }}
         />
      </View>
   );
}

export default WeeklyCalendar;
