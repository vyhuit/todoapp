import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Title } from "react-native-paper";
import { useSelector } from "react-redux";
import { colors } from "../../Static/Js/constants";

const TodayStatusCard = ({ name = "NoName", today, todayTasks }) => {
  const taskList = useSelector((state) => state.task.taskList);
  const completedTasks = taskList.filter(
    (task) => task.isDone === true
  ).length;
  return (
    <View style={styles.container}>
      <View style={styles.paragraph}>
        <View style={styles.info}>
          <Text>
            Good Morning <Text style={{ fontWeight: "bold" }}>{name}</Text>
          </Text>
          <Title
            style={[
              styles.boldText,
              {
                color: colors.primary,
              },
            ]}
          >
            Today
          </Title>
          <Text
            style={[
              styles.boldText,
              {
                color: colors.blackLabel,
                fontSize: 22,
                textTransform: "capitalize",
              },
            ]}
          >
            {today}
          </Text>
        </View>
        {todayTasks.length === 0 ? (
          <View style={styles.complete}>
            <Text style={{ color: colors.green }}>No tasks</Text>
          </View>
        ) : (
          <View style={styles.complete}>
            <Text style={{ color: colors.green }}>Completed</Text>
            <Text
              style={{ fontSize: 20, fontWeight: "bold", color: colors.green }}
            >
              {completedTasks} /{" "}
              <Text style={{ color: colors.priority1 }}>
                {todayTasks.length}
              </Text>
            </Text>
          </View>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
    backgroundColor: colors.defaultBackground,
    //  flex: 1,,
  },
  paragraph: {
    backgroundColor: colors.white,
    //  height: "20%",
    padding: 20,
    shadowColor: colors.borderGrayColor,
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 3,
    flexDirection: "row",
    justifyContent: "space-between",
    fontSize: 18,
  },
  info: {
    //  gap: "1,5rem",
    fontSize: 18,
  },
  boldText: {
    fontWeight: "bold",
    textTransform: "uppercase",
  },
  complete: {
    //  gap: "0.5rem",
    justifyContent: "center",
    color: colors.green,
    alignItems: "center",
  },
});

export default TodayStatusCard;
