import moment from "moment";
import React, { useEffect, useState } from "react";
import {
   Keyboard,
   ScrollView,
   Text,
   TouchableOpacity,
   TouchableWithoutFeedback,
   View,
} from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { TextInput } from "react-native-paper";
import Select from "react-native-picker-select";
import { useDispatch, useSelector } from "react-redux";
import {
   addNewTask,
   setCate,
   setCurrentNewTask,
   updateTask,
} from "../Redux/actions/taskActions";
import { colors, log } from "../Static/Js/constants";
import PickTime from "./Components/PickTime";
import Priority from "./Components/Priority";
import BottomButton from "./General/BottomButton";
import HeaderNavigation from "./General/HeaderNavigation";

const NewTaskScreen = ({ navigation }) => {
   const dispatch = useDispatch();
   const cTask = useSelector((state) => state.task.currentTaskTemplate);
   const cateList = useSelector((state) => state.task.categoryList);
   const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

   const onAddNewTask = async () => {
      if (cTask.name != "" && cTask.deadline != null) {
         dispatch(addNewTask(cTask));
         dispatch(
            setCurrentNewTask({
               id: null,
               name: "",
               priority: 1,
               notificationType: 1,
               description: "",
               deadline: null,
               cateId: null,
               isLater: false,
               isStar: false,
               updateDeadline: null,
               isDone: false,
            })
         );
         navigation.navigate("Home");
      } else {
         let newTask = {
            cateId: "fb44d7ea-dcd9-49a4-b3e5-16affa8ba934",
            isDone: false,
            deadline: moment().add(2, 'days'),
            description: "Have to meet him because i want to show him my latest app design in person Also need to ask for advice on these",
            isLater: false,
            isStar: true,
            notificationType: 3,
            priority: 2,
            name: "task ngay 14",
            updateDeadline: null
         }
         dispatch(addNewTask(newTask));
         navigation.navigate("Home");
      }
   };

   const onUpdateTask = async () => {
      const newTask = {
         taskName: getValues("taskName"),
         description: getValues("description"),
         category: cTask.cateId,
         dateTime: deadline,
         priority: cTask.priority,
         notification: notification,
         completed: false,
         id: 1,
      };
      await dispatch(updateTask(newTask));
      navigation.navigate("Home");
   };

   const onChangeField = (data) => {
      dispatch(setCurrentNewTask(data));
   };

   const showDatePicker = () => {
      setDatePickerVisibility(true);
      today = new Date();
   };

   const hideDatePicker = () => {
      setDatePickerVisibility(false);
   };

   const handleConfirm = (date) => {
      let stringSelected = moment(date).format("YYYY-MM-DD HH:mm");
      onChangeField({ ...cTask, deadline: stringSelected });
      hideDatePicker();
   };

   const getCateLabel = () => {
      if (cTask.cateId) {
         let item = cateList.find((item) => item.value == cTask.cateId);
         return item ? item.label : "Please seletect category";
      } else return "Please seletect category";
   };

   const [dateTime, setDateTime] = useState('2021-08-12 | 08:00');

   return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
         <View style={{ flex: 1, paddingBottom: 100 }}>
            <HeaderNavigation
               showNewTask={false}
               title="New Task"
               isBackable={true}
               showSearchBar={false}
               onPressButton={() => {
                  dispatch(
                     setCurrentNewTask({
                        ...cTask,
                        cateId: null,
                     })
                  );
                  navigation.navigate("Home");
               }}
            />
            <ScrollView style={{ height: "100%" }}>
               <TextInput
                  style={{
                     backgroundColor: colors.white,
                     fontWeight: "normal",
                     paddingHorizontal: 18,
                  }}
                  placeholder="Task name"
                  onChangeText={(value) => {
                     onChangeField({ ...cTask, name: value });
                  }}
                  value={cTask.name}
                  label="Task Name"
               />
               <TextInput
                  style={{
                     backgroundColor: colors.white,
                     fontWeight: "normal",
                     paddingHorizontal: 18,
                  }}
                  placeholder="Description"
                  onChangeText={(value) => {
                     onChangeField({ ...cTask, description: value });
                  }}
                  value={cTask.description}
                  label="Description"
               />
               <View style={{ backgroundColor: colors.white }}>
                  <TouchableOpacity
                     onPress={() => {
                        navigation.navigate("Category", {
                           cate: cTask.cateId,
                        });
                     }}
                  >
                     <View
                        style={{
                           height: 60,
                           backgroundColor: colors.white,
                           borderBottomColor: colors.borderGrayColor,
                           borderBottomWidth: 1,
                           justifyContent: "center",
                           paddingHorizontal: 10,
                        }}
                     >
                        <Text
                           style={{
                              color: colors.borderGrayColor,
                              backgroundColor: colors.white,
                              fontSize: 16,
                              paddingVertical: 20,
                              fontWeight: "normal",
                              paddingHorizontal: 8,
                           }}
                        >
                           {getCateLabel()}
                        </Text>
                     </View>
                  </TouchableOpacity>
               </View>
               {Platform.OS === 'ios' ?
                  <View style={{ backgroundColor: colors.white }}>
                     <TouchableOpacity onPress={showDatePicker}>
                        <View
                           style={{
                              backgroundColor: colors.white,
                              borderBottomColor: colors.borderGrayColor,
                              borderBottomWidth: 1,
                              justifyContent: "center",
                              paddingHorizontal: 10,
                           }}
                        >
                           <Text
                              style={{
                                 color: colors.borderGrayColor,
                                 backgroundColor: colors.white,
                                 fontSize: 16,
                                 paddingVertical: 20,
                                 fontWeight: "normal",
                                 paddingHorizontal: 8,
                              }}
                           >
                              {cTask.deadline ? cTask.deadline : "Select deadline"}
                           </Text>
                        </View>
                     </TouchableOpacity>
                     <DateTimePickerModal
                        isVisible={isDatePickerVisible}
                        timePickerModeAndroid="clock"
                        datePickerModeAndroid="calendar"
                        mode="datetime"
                        minimumDate={new Date()}
                        onConfirm={handleConfirm}
                        onCancel={hideDatePicker}
                        date={cTask.deadline ? cTask.deadline : new Date()}
                     />
                  </View>
                  : <PickTime dateTimex={null} setDateTime={setDateTime} />}

               <Text
                  style={{
                     fontWeight: "600",
                     paddingHorizontal: 18,
                     paddingVertical: 15,
                     fontSize: 17,
                     color: colors.blackLabel,
                     backgroundColor: colors.background,
                     width: "100%",
                  }}
               >
                  Priority
               </Text>
               <Priority
                  priority={cTask.priority}
                  setPriority={(val) => {
                     onChangeField({ ...cTask, priority: val });
                  }}
               />
               <Text
                  style={{
                     fontWeight: "600",
                     paddingHorizontal: 18,
                     paddingVertical: 15,
                     fontSize: 17,
                     color: colors.blackLabel,
                     backgroundColor: colors.background,
                  }}
               >
                  Notification Time
               </Text>
               <View
                  style={{
                     paddingHorizontal: 18,
                     height: 60,
                     backgroundColor: colors.white,
                     display: "flex",
                     justifyContent: "center",
                  }}
               >
                  <Select
                     style={{ height: "100%", fontSize: 30 }}
                     placeholder={"Select time to send push notification"}
                     onValueChange={(value) => {
                        onChangeField({ ...cTask, notification: value });
                     }}
                     items={[
                        { label: "10 munutes before", value: 1 },
                        { label: "1 hour before", value: 2 },
                        { label: "1 day before", value: 3 },
                     ]}
                     value={cTask.notificationType}
                  />
               </View>
            </ScrollView>
            <View style={{ position: "absolute", bottom: 0, width: "100%" }}>
               {cTask.id ? (
                  <BottomButton text="Update" onPress={onUpdateTask} />
               ) : (
                  <BottomButton text="Add New Task" onPress={onAddNewTask} />
               )}
            </View>
         </View>
      </TouchableWithoutFeedback>
   );
};

export default NewTaskScreen;
