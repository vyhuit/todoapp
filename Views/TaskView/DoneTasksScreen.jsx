import React from "react";
import { Text, View } from "react-native";
import { colors } from "../../Static/Js/constants";
import HeaderNavigation from "../General/HeaderNavigation";


const DoneTasksScreen = ({ navigation }) => {
   return (
      <View style={{ flex: 1 }}>
         <HeaderNavigation title="done Tasks" isBackable={true} onPressButton={() => navigation.navigate("Home")} />
         <View style={{ flex: 1, backgroundColor: colors.green }}>
            <Text>Đây là trang new Task nè</Text>
         </View>
      </View>
   );
};

export default DoneTasksScreen;
