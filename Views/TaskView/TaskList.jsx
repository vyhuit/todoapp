import moment from "moment";
import React from "react";
import { SectionList, StyleSheet, Text, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { updateTask } from "../../Redux/actions/taskActions";
import { colors, log } from "../../Static/Js/constants";
import EmptyListView from "../General/EmptyListView";
import TaskItem from "../General/TaskItem";

export const HRLine = ({ opacity = 0.1 }) => (
   <View
      style={{
         width: "100%",
         borderBottomColor: "gray",
         borderBottomWidth: 0.5,
         opacity: opacity,
      }}
   />
);
export default function TaskList({
   data,
   parentName,
   extenTaskAction,
   doneTaskAction,
}) {
   // Data format
   // data =[
   //    {
   //      title: "",
   //      data: []
   //    },...
   // ]

   return (
      <SectionList
         sections={data}
         keyExtractor={(item, index) => item + index}
         renderItem={(data) => {
            return (
               <TaskItem
                  task={data.item}
                  expandTaskClick={(item) => extenTaskAction(item)}
               />
            );
         }}
         renderSectionHeader={(item) => {
            return (
               <Text style={styles.header}>
                  {/* {item.section.title === "null"
                     ? ""
                     : item.section.title === "Today"
                        ? "Today"
                        : moment(item.section.data[0].deadline).format("DD MMMM")} */}
                  {item.section.day === 'today' ? 'Today' : moment(item.section.day).format('DD MMMM')}
               </Text>
            );
         }}
         ItemSeparatorComponent={() => <HRLine />}
         ListEmptyComponent={() => <EmptyListView nextTaskTimeString="" />}
      />
   );
}

const styles = StyleSheet.create({
   header: {
      paddingTop: 10,
      textAlign: "center",
      paddingBottom: 10,
      color: colors.secondary,
      fontSize: 14,
   },
   item: {
      width: "100%",
      paddingHorizontal: 15,
      paddingVertical: 5,
      backgroundColor: "white",
      height: 60,
      display: "flex",
      justifyContent: "center",
   },
   name: {
      flexGrow: 1,
      width: "100%",
   },
});
