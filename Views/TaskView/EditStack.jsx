import {
   createStackNavigator,
   TransitionPresets
} from "@react-navigation/stack";
import React from "react";
import SelectCategory from "../Components/SelectCategory";
import EditTaskScreen from './EditTaskScreen';

const Stack = createStackNavigator();

export default function EditStack() {
   return (
      <Stack.Navigator
         initialRouteName="EditTask"
         screenOptions={{
            gestureDirection: "horizontal",
            gestureEnabled: true,
            ...TransitionPresets.SlideFromRightIOS,
         }}
      >
         <Stack.Screen name="EditTask" component={EditTaskScreen} options={navOptionHandler} />
         <Stack.Screen
            name="Category"
            component={SelectCategory}
            options={navOptionHandler}
         />
      </Stack.Navigator>
   );
}

const navOptionHandler = () => ({
   headerShown: false,
});
