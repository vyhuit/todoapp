// id, name, createDate, endDate, description, cateId, priority, userId
import moment from "moment";
import React, { useEffect, useState } from "react";
import { Alert, ScrollView, StyleSheet, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { updateTask } from "../../Redux/actions/taskActions";
import { colors, compareEqualDay, log } from "../../Static/Js/constants";
import TodayStatusCard from "../General/TodayStatusCard";
import TaskList from "./TaskList";

const today = moment().format("DD MMM, YYYY");
export default function DailyTasks() {
  const taskList = useSelector((state) => state.task.taskList);
  const dispatch = useDispatch();
  let todayTask = [];
  if (taskList.length > 0) {
    todayTask = taskList.filter((x) => {
      return (
        (compareEqualDay(x.deadline, new Date()) && !x.isLater) ||
        (x.isLater && compareEqualDay(x.updateDeadline, new Date()))
      );
    });
  }
  const expanTaskClick = (item) => {
    Alert.alert(
      "Extend Your Task Deadline",
      "Your task will be extended by 7 days?",
      [
        {
          text: "Cancel",
          onPress: () => {},
          style: "cancel",
        },
        {
          text: "OK",
          onPress: () =>
            dispatch(
              updateTask({
                ...item,
                isLater: true,
                updateDeadline: moment(item.deadline)
                  .add(7, "days")
                  .format("YYYY-MM-DD HH:mm"),
              })
            ),
        },
      ]
    );
  };
  return (
    <ScrollView>
      <View style={styles.taskViewContainer}>
        <TodayStatusCard
          name="Administrator"
          today={today}
          todayTasks={todayTask}
        />
        <View
          style={{
            flex: 1,
            backgroundColor: colors.defaultBackground,
          }}
        >
          <TaskList
            parentName="daily"
            extenTaskAction={(item) => {
              expanTaskClick(item);
            }}
            data={
              todayTask.length > 0 ? [{ title: "null", data: todayTask }] : []
            }
          />
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  taskViewContainer: {
    flex: 1,
  },
});
