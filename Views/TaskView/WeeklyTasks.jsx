import React from "react";
import { StyleSheet, View, Text } from "react-native";
import { useSelector } from "react-redux";
import WeeklyCalendar from "../General/WeeklyCalendar";
import WeeklyTaskDetail from "../General/WeeklyTaskDetail";
import TaskList from "./TaskList";

export default function WeeklyTasks() {
   const taskList = useSelector((state) => state.task.taskList);
   let data = [];
   return (
      <View style={styles.taskViewContainer}>
         <WeeklyCalendar taskList={taskList} />
         <WeeklyTaskDetail taskList={taskList} />
         {/* <TaskList
            data={taskList.length > 0 ? [{ title: "null", data: taskList }] : []}
         /> */}
      </View>
   );
}
const styles = StyleSheet.create({
   taskViewContainer: { flex: 1 },
});
