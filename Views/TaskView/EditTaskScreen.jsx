import { useNavigation } from "@react-navigation/native";
import React from "react";
import { Controller, useForm } from "react-hook-form";
import {
  Keyboard,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { TextInput } from "react-native-paper";
import { useSelector } from "react-redux";
import { colors } from "../../Static/Js/constants";
import HeaderNavigation from "../General/HeaderNavigation";

const EditTaskScreen = () => {
  const { control } = useForm();
  const navigation = useNavigation();
  const task = useSelector((state) =>
    state.task.taskList.find((t) => t.id === 1)
  );
  const { category, taskName, description, notification, dateTime, priority } =
    task;

  // const [category, setCategory] = useState("")
  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View style={{ flex: 1 }}>
        <HeaderNavigation
          title="Edit Task"
          isBackable={true}
          onPressButton={() => navigation.goBack()}
        />
        <KeyboardAwareScrollView style={{ flex: 1 }}>
          <Controller
            control={control}
            name="taskName"
            render={({ field }) => (
              <TextInput
                defaultValue="Meet Joshoep"
                label="Task Name"
                mode="flat"
                style={{
                  fontWeight: "bold",
                  fontSize: 20,
                  backgroundColor: colors.white,
                }}
                {...field}
              />
            )}
          />
          <Controller
            control={control}
            name="description"
            render={({ field }) => (
              <TextInput
                defaultValue={`Have to meet him becase i want to show him my latest app design in person\nAlso need to ask for advice`}
                label="Task Name"
                mode="flat"
                multiline
                style={{ backgroundColor: colors.white }}
                {...field}
              />
            )}
          />
          <View style={{ backgroundColor: colors.white }}>
            <TouchableOpacity onPress={() => navigation.navigate("Category")}>
              <View
                style={{
                  height: 50,
                  backgroundColor: colors.white,
                  borderBottomColor: colors.borderGrayColor,
                  borderBottomWidth: 1,
                  justifyContent: "center",
                  paddingHorizontal: 10,
                }}
              >
                <Text
                  style={{
                    color: colors.black,
                    fontSize: 16,
                    paddingVertical: 20,
                  }}
                >
                  Select category
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default EditTaskScreen;
