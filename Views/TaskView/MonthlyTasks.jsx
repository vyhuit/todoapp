import moment from "moment";
import React, { useEffect, useState } from "react";
import { ScrollView, StyleSheet, View } from "react-native";
import { Calendar } from "react-native-calendars";
import { useDispatch, useSelector } from "react-redux";
import { colors } from "../../Static/Js/constants";
import MonthlyTaskDetail from "../General/MonthlyTaskDetail";
const INITIAL_DATE = moment().add(1, 'days').format('YYYY-MM-DD');

export default MonthlyTasks = () => {
   const [selected, setSelected] = useState(INITIAL_DATE);
   const [dayTask, setDayTask] = useState({});
   const task = useSelector(state => state.task);
   const { taskList, firstDayOfMonth, lastDayOfMonth } = task;
   const dispatch = useDispatch();


   useEffect(() => {
      let markDate = {};
      taskList.forEach((e) => {
         let date = moment(e.deadline).format("YYYY-MM-DD");
         markDate[date] = {
            marked: true,
         };
      });
      setDayTask(markDate);
   }, [taskList]);


   // const onDayPress = (day) => {
   //    // setSelected(day.dateString);
   //    alert(
   //       moment("27 Feb, 2018 04:00 PM".slice(0, 12)).format('YYYY-MM-DD') + "Clicked. Getting all tasks of " +
   //       day.day +
   //       "-" +
   //       day.month +
   //       "-" +
   //       day.year
   //    );
   // };
   const [{ key, theme }, setTheme] = useState({
      key: 'dark', theme: {
         selectedDayTextColor: 'red',
      }
   })

   return (
      <ScrollView showsVerticalScrollIndicator={false}>
         <View>
            <Calendar
               // onDayPress={onDayPress}
               style={styles.calendar}
               current={selected}
               markedDates={dayTask}
               onMonthChange={(month) => {
                  let firstDay = moment(month.dateString).startOf('month').format("YYYY-MM-DD")
                  let lastDay = moment(month.dateString).endOf('month').format("YYYY-MM-DD")
                  dispatch({ type: 'CHANGE_MONTHLY', payload: { firstDay: firstDay, lastDay: lastDay } })
               }}
               key={key}
               onDayPress={(day) => {
                  setSelected(day.dateString);
                  let firstDay = moment(day.dateString).format("YYYY-MM-DD")
                  dispatch({ type: 'CHANGE_MONTHLY', payload: { firstDay: firstDay, lastDay: firstDay } })
               }}
            />
            <View>
               <MonthlyTaskDetail />
            </View>
         </View>
      </ScrollView>
   );
};

const styles = StyleSheet.create({
   calendar: {
      marginBottom: 0,
   },
});
