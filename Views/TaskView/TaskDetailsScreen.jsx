import moment from "moment";
import React from "react";
import { Text, View } from "react-native";
import { useSelector } from "react-redux";
import { colors } from "../../Static/Js/constants";
import HeaderNavigation from "../General/HeaderNavigation";

const TaskDetailsScreen = ({ route, navigation }) => {
   const cateList = useSelector(state => state.task.categoryList);
   const { params } = route;

   const getCateLabel = () => {
      let item = cateList.find((item) => item.value == params.cateId);
      return item ? item.label : "No category";
   };
   return (
      <View style={{ flex: 1 }}>
         <HeaderNavigation title="TaskDetails" isBackable={true} onPressButton={() => navigation.goBack()} />
         <View style={{ flex: 1, backgroundColor: colors.defaultBackground, padding: 20 }}>
            <View style={{ backgroundColor: colors.white, padding: 20 }}>
               <Text style={{ fontWeight: 'bold', fontSize: 20 }}>{params.name}</Text>

               <Text style={{ color: colors.borderGrayColor }}>{moment(params.deadline).format('DD MMM, YYYY | h:mm A')}</Text>
               <View style={{ paddingVertical: 20 }}>
                  <Text>{params.description}</Text>
               </View>
               <Text>
                  <Text style={{ fontWeight: 'bold' }}>Category:</Text>
                  &nbsp; {getCateLabel()}
               </Text>
            </View>
         </View>
      </View>
   );
};

export default TaskDetailsScreen;
