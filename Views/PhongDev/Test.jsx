import * as React from "react";
import {
  View,
  StyleSheet,
  Dimensions,
  StatusBar,
  TouchableOpacity,
  Text,
  Animated,
} from "react-native";
import { TabView, SceneMap, TabBar } from "react-native-tab-view";
import { colors } from "../../Static/Js/constants";
import DailyTasks from "./../TaskView/DailyTasks";
import WeeklyTasks from "./../TaskView/WeeklyTasks";

const FirstRoute = () => (
  <View style={[styles.scene, { backgroundColor: "#ff4081" }]} />
);

const SecondRoute = () => (
  <View style={[styles.scene, { backgroundColor: "#673ab7" }]} />
);

const initialLayout = { width: Dimensions.get("window").width };

const renderScene = ({ route }) => {
  switch (route.key) {
    case "daily":
      return <DailyTasks />;
    case "weekly":
      return <WeeklyTasks />;
    case "monthly":
      return <View style={[styles.scene, { backgroundColor: "#673ab7" }]} />;
    default:
      return null;
  }
};
export default function Test() {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: "daily", title: "Daily" },
    { key: "weekly", title: "Weekly" },
    { key: "monthly", title: "Monthly" },
  ]);

  const _handleIndexChange = (index) => setIndex(index);

  _renderTabBar = (props) => {
    const inputRange = props.navigationState.routes.map((x, i) => i);

    return (
      <View style={styles.tabBar}>
        {props.navigationState.routes.map((route, i) => {
          const opacity = props.position.interpolate({
            inputRange,
            outputRange: inputRange.map((inputIndex) =>
              inputIndex === i ? 1 : 0.5
            ),
          });

          return (
            <TouchableOpacity
              style={styles.tabItem}
              onPress={() => setIndex(i)}
            >
              <Animated.Text
                style={[
                  { opacity },
                  {
                    color: colors.white,
                    fontSize: 20,
                    fontWeight: "bold",
                    letterSpacing: 2,
                  },
                ]}
              >
                {route.title}
              </Animated.Text>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

  return (
    <TabView
      navigationState={{ index, routes }}
      renderScene={renderScene}
      onIndexChange={_handleIndexChange}
      initialLayout={initialLayout}
      renderTabBar={_renderTabBar}
      style={styles.container}
    />
  );
}

const styles = StyleSheet.create({
  container: {
    //  marginTop: StatusBar.currentHeight,
  },
  scene: {
    flex: 1,
  },
  tabBar: {
    flexDirection: "row",
  },
  tabItem: {
    flex: 1,
    alignItems: "center",
    backgroundColor: colors.primary,
    padding: 16,
  },
});
