import React from "react";
import { Text, TouchableOpacity, View } from "react-native";
import { colors } from "../../Static/Js/constants";

const CustomHeader = ({ navigation, title = "Thêm title vào" }) => {
  return (
    <View
      style={{
        flexDirection: "row",
        height: 50,
        backgroundColor: colors.primary,
      }}
    >
      <View>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Image
            style={{ width: 30, height: 30, marginLeft: 5 }}
            source={require("../../Static/Images/icons/left-arrow.png")}
            resizeMode="contain"
          />
        </TouchableOpacity>
      </View>
      <View
        style={{
          flex: 1.5,
          justifyContent: "center",
        }}
      >
        <Text style={{ textAlign: "center" }}>{title}</Text>
      </View>
      <View
        style={{
          flex: 1,
          justifyContent: "center",
        }}
      ></View>
    </View>
  );
};

export default CustomHeader;
